<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller {
    //metodos
    public function home() {
    	$dueño = "Gustavo Adolfo Ramírez Apache";
		$links = [
			'https://beerndev.co' => "Beer & dev",
			'https://facebook.com' => "Pagina facebbok"
		];
		$messages = [
			[
				'id'      => 1,
				'content' => 'Este es mi mensaje',
				'image'   => 'http://lorempixel.com/600/338?1',
			],
			[
				'id'      => 2,
				'content' => 'Este es mi mensaje total',
				'image'   => 'http://lorempixel.com/600/338?2'
			],
			[
				'id'      => 3,
				'content' => 'Este es un mensaje nuevo',
				'image'   => 'http://lorempixel.com/600/338?3'
			],
			[
				'id'      => 4,
				'content' => 'Este es el 4to mensaje',
				'image'   => 'http://lorempixel.com/600/338?4'
			]			

		];
	    return view('welcome', [
	    	'links' => $links,
	    	'messages' => $messages
	    	//'ceo' => $dueño
	    ]);
	}
	//metodos
	public function about() {
		return view('about');
	}
}
